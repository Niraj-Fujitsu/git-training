/**************************************************
Name    : giic_BarterWrapper 
Author  : Ashok Kumar test
Purpose : This class is used for collecting value for SalesOrderPayment Object.
Created Date: 28/09/2016
Modification History:
    <initials> - <date> - <reason for update>
*****************************************************/
public class giic_BarterWrapper {
    public class SalesOrderInformation{
        @AuraEnabled
        public gii__SalesOrder__c objSalesOrder_1;
       
        @AuraEnabled
        public gii__SalesOrderPayment__c objSalesOrderPayment;
        @AuraEnabled
        public gii__SalesOrderPayment__c objSalesOrderPaymentRecord;
        
        /****************************************Sales Quote CR Changes **************************/
        
        @AuraEnabled
        public gii__SalesQuote__c objSalesQuote;
        @AuraEnabled
        public giic_BarterQuoteLines__c objSalesQuoteBarter;
        @AuraEnabled
        public giic_BarterQuoteLines__c objSalesQuoteBarterRecord;
        
        
        /****************************************End Here **************************/
        @AuraEnabled
        public List<String> lstBarterModelOptions;
        @AuraEnabled
        public map<String,string> mapOfftaker; 
        
        @AuraEnabled
        public map<String,string> mapPriceAgreement; 
        
        @AuraEnabled
        public map<String,gii__BarterCatalogLine__c> mapOfftakerType; 
        @AuraEnabled
        public map<String,string> mapServiceProvider;
        @AuraEnabled
        public map<String,gii__BarterCatalogLine__c> mapServiceProviderObj; 
        /**************************
		INC001
		**************************/
        @AuraEnabled
        public map<String,list<string>> mapServiceProviderServicesAfter;        
		@AuraEnabled
        public map<String,list<string>> mapServiceProviderServicesbefore;
        /**************************
		INC001
		**************************/	 
        @AuraEnabled
        public list<String> lstSPServiceTypeCommodity;
        @AuraEnabled
        public List<String> comodityCategory;
        @AuraEnabled
        public map<String,string> comodityType;
        @AuraEnabled
        public map<String,gii__BarterCatalogLine__c> mapBrokerRates;
        @AuraEnabled
        public List<String> serviceType;
        
        @AuraEnabled
        public map<String,string> commodityName;
        @AuraEnabled
        public map<String,string> delivaryWarehouseMap;
        @AuraEnabled
        public List<String> priceSeasonList;
        @AuraEnabled
        public List<String> harvestRegionList;
        @AuraEnabled
        public List<String> certificateNumberList;
        @AuraEnabled
        public List<String> priceAgreement;
        @AuraEnabled
        public List<String> taxLocation;
        @AuraEnabled
        public List<gii__TaxRate__c> taxLocationList;
        @AuraEnabled
        public map<id,gii__TaxRate__c> taxLocationMap;
        @AuraEnabled
        public List<gii__BarterCatalogLine__c> brokerProviderList;
        //@AuraEnabled
        //public List<String> priceMethod;
        @AuraEnabled
        public boolean isNutrade;
        @AuraEnabled
        public boolean isRatesValue;
        @AuraEnabled
        public Decimal totalTax;
        @AuraEnabled
        public Decimal brokerPercentageRate;
        @AuraEnabled
        public Decimal BrokerProviderLength;
        @AuraEnabled
        public String attributeVal;
        @AuraEnabled
        public boolean isOffTakerNutrade;
        @AuraEnabled
        public boolean isManager;
        @AuraEnabled
        public boolean isBrokerService;
        @AuraEnabled
        public boolean isDisplayBroker;
        @AuraEnabled
        public Integer  numberProdSelected;
        /**************************
		INC003
		**************************/
		@AuraEnabled
        public boolean isChannelBarterAfter;
        @AuraEnabled        
		public boolean isChannelBarterBefore;
		/**************************
		INC003
		**************************/		 
        @AuraEnabled
        public boolean isServiceProvider;
        @AuraEnabled
        public List<String> lstCommodityAlternateUOM;
        @AuraEnabled
        public String CommodityUOM;
        @AuraEnabled
        public Decimal commodityActualNetPrice; 
        @AuraEnabled
        public Decimal commodityRefNetPrice; 
        @AuraEnabled
        public string barterTemplateId; 
        @AuraEnabled
        public Integer marginAmount;
        @AuraEnabled
        public boolean isFirstLoadBefore;        
		@AuraEnabled
        public boolean isFirstLoadAfter;
        
        
        public SalesOrderInformation(){
             
            this.objSalesOrder=new gii__SalesOrder__c();
            this.objSalesOrderPaymentRecord =giic_BarterHelper.getIntializeSOP();
           	this.objSalesOrderPayment=giic_BarterHelper.getIntializeSOP();
           	 /****************************************Sales Quote CR Changes **************************/
           	this.objSalesQuoteBarterRecord =giic_BRQuoteBarterHelper.getIntializeSOP();
           	this.objSalesQuoteBarter=giic_BRQuoteBarterHelper.getIntializeSOP();
           	this.objSalesQuote=new gii__SalesQuote__c();
           	/****************************************End Here **************************/
            this.isNutrade=false;
            this.barterTemplateId='';
            this.marginAmount = 0;
            this.isFirstLoad= true;
            this.isOffTakerNutrade = false;
            this.isRatesValue = false;
            this.isDisplayBroker = false;
            this.totalTax = 0.00;
            this.commodityActualNetPrice =0.00;
            this.commodityRefNetPrice=0.00;
            this.isManager = false;
            this.isChannelBarter = false;
            this.isServiceProvider = false;
            this.numberProdSelected = 0;
            this.isBrokerService = false;
            this.brokerPercentageRate =0.0;
            this.attributeVal = '';
            this.BrokerProviderLength = 0;
            this.CommodityUOM ='';
            this.lstCommodityAlternateUOM = new List<String>();
            this.taxLocationList = new List<gii__TaxRate__c>();
            this.taxLocationMap= new map<Id,gii__TaxRate__c>();
            this.brokerProviderList = new List<gii__BarterCatalogLine__c>();
            this.mapOfftaker=new map<String,string>();
            this.mapServiceProvider=new map<String,string>();
            this.lstSPServiceTypeCommodity=new list<string>();
            this.mapBrokerRates=new map<String,gii__BarterCatalogLine__c>();
            this.mapServiceProviderServices=new map<String,list<string>>();
            this.lstBarterModelOptions=giic_CommonUtilityHelper.getPicklistOptions('giic_BarterModel__c','gii__SalesOrderPayment__c');
            this.comodityCategory=new list<string>();
            this.comodityType=new map<String,string>();
            this.serviceType=new list<string>();
            this.mapPriceAgreement = new map<String,String>();
            this.priceAgreement = giic_CommonUtilityHelper.getPicklistOptions('giic_ContractPriceAgreement__c','gii__SalesOrderPayment__c');
            //this.priceMethod = giic_CommonUtilityHelper.getPicklistOptions('gii__PaymentMethod__c','gii__SalesOrderPayment__c');
            this.mapOfftakerType=new map<String,gii__BarterCatalogLine__c>();
            this.mapServiceProviderObj = new map<String, gii__BarterCatalogLine__c>();
            this.commodityName = new map<String,string>();
            this.taxLocation = new List<String>();
            this.priceSeasonList = new List<String>();
            this.harvestRegionList = new List<String>();
            this.certificateNumberList = new List<String>();
            this.delivaryWarehouseMap = new map<String,String>();
        }
    }
}