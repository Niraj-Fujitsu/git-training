/**************************************************
Name    : giic_BarterHelper
Author  : Ashok Kumar
Purpose : This class is used for every common functionality/method. 
Created Date: 28/09/2016
Modification History:
    <initials> - <date> - <reason for update>
*****************************************************/
public without sharing class giic_BarterHelper { 
    
    /**************************************************
    Method      : getSalesOrderInfo
    Purpose     : This Method return wrapper class after setting property value.
    *****************************************************/
    @AuraEnabled
    public static giic_BarterWrapper.SalesOrderInformation getSalesOrderInfo(String barterTempId){
        giic_BarterWrapper.SalesOrderInformation objSalesOrderInfo=new giic_BarterWrapper.SalesOrderInformation();
        
        /**************************************
         * get all Service Provider and Off taker
         * *******************************/
        for( gii__BarterCatalogLine__c vasPriceEntry : [Select id,giic_OffTaker__c,giic_OffTakerName__c,giic_Services__c,gii__Type__c,gii__BarterCatalog__r.giic_External_Id__c,
                                                        gii__BarterCatalog__r.Name,gii__BarterCatalog__r.gii__Type__c,giic_LSP__c,giic_ServiceProviderName__c,giic_Attribute7__c 
                                                        from gii__BarterCatalogLine__c 
                                                        where (gii__BarterCatalog__r.giic_External_Id__c='OFFTAKER' AND giic_OffTakerStatus__c= 'A') 
                                                        OR (gii__BarterCatalog__r.giic_External_Id__c='SERVICEPROVIDER' ) or gii__Type__c = 'Logistic Service' ORDER BY giic_OffTakerName__c ASC LIMIT 50000]) {
            system.debug('****vasPriceEntry***'+vasPriceEntry);
                if(vasPriceEntry.gii__BarterCatalog__r.giic_External_Id__c == 'OFFTAKER' && vasPriceEntry.giic_Attribute7__c != null){
                   (objSalesOrderInfo.mapOfftaker).put(vasPriceEntry.giic_OffTaker__c,vasPriceEntry.giic_OffTakerName__c);
                    (objSalesOrderInfo.mapOfftakerType).put(vasPriceEntry.giic_OffTaker__c,vasPriceEntry);
            }else if(vasPriceEntry.gii__BarterCatalog__r.giic_External_Id__c=='SERVICEPROVIDER' && vasPriceEntry.gii__Type__c =='Service Provider'){
                   (objSalesOrderInfo.mapServiceProvider).put(vasPriceEntry.giic_LSP__c,vasPriceEntry.giic_ServiceProviderName__c);
                (objSalesOrderInfo.mapServiceProviderObj).put(vasPriceEntry.giic_LSP__c,vasPriceEntry);
                
            } 
            else if(vasPriceEntry.gii__Type__c == 'Logistic Service' && vasPriceEntry.gii__BarterCatalog__r.giic_External_Id__c !='SERVICEPROVIDER' ){
                if(objSalesOrderInfo.mapServiceProviderServices.containskey(vasPriceEntry.giic_LSP__c)){
                    set<string> setServices=new set<string>();
                    list<string> lstServices=new list<string>();
                    setServices.addall(objSalesOrderInfo.mapServiceProviderServices.get(vasPriceEntry.giic_LSP__c));
                    setServices.add(vasPriceEntry.giic_Services__c);
                    lstServices.addAll(setServices);
                    objSalesOrderInfo.mapServiceProviderServices.put(vasPriceEntry.giic_LSP__c,lstServices);
                    
                }
                else{
                    objSalesOrderInfo.mapServiceProviderServices.put(vasPriceEntry.giic_LSP__c,new list<string>{vasPriceEntry.giic_Services__c});
                    //objSalesOrderInfo.mapServiceProviderCommodityServices.put(vasPriceEntry.giic_LSP__c + '~' + vasPriceEntry.giic_Services__c,new list<string>{vasPriceEntry.gii__VAS1__r.Name});
                }
                objSalesOrderInfo.lstSPServiceTypeCommodity.add(vasPriceEntry.giic_LSP__c + '~' + vasPriceEntry.giic_Services__c +'~'+vasPriceEntry.gii__BarterCatalog__r.Name);
            }
         }
        
        /************************
    * Incident INC0545719  
    * */
        objSalesOrderInfo.mapPriceAgreement = giic_createsalesorderheaderctrl.getPicklistOptionsMap('giic_ContractPriceAgreement__c','gii__SalesOrderPayment__c');
        
        /*********************************
         * getting Saleorder record from template id
         * *******************************/
        if(!String.isBlank(barterTempId)){
            list<gii__SalesOrderPayment__c> lstSOP= getSalesOrderPaymentInfo(new map<string,object>{'salesOrderPaymentId'=>barterTempId});
            system.debug('lstSOP****'+ lstSOP );
            if(lstSOP != null && lstSOP.size() > 0){
                Map<String, Object> fieldsToValue = lstSOP[0].getPopulatedFieldsAsMap();
                for (String fieldName : fieldsToValue.keySet()){
                    if(!fieldName.equalsIgnoreCase('Id'))
                        objSalesOrderInfo.objSalesOrderPaymentRecord.put(fieldName,fieldsToValue.get(fieldName));
                }
    
                system.debug('soPyament after clone****'+ objSalesOrderInfo.objSalesOrderPaymentRecord );
            }
        }
        
        /*********************************
         * get logged in user profile and dislay the calcaltion based on the profile
         * *******************************/
        String profile = [select Profile.Name from User where Id = :Userinfo.getUserId()].Profile.Name;
        string profileName=System.Label.giic_ProfileNameSubString;
        list<string> lstProfileName=profileName.split(',');
        set<string> setProfileNames=new set<string>();
        setProfileNames.addAll(lstProfileName);
        if(setProfileNames.contains(profile))
            objSalesOrderInfo.isManager = profile.endsWith(System.Label.giic_ProfileNameSubString);  
        
        
        /*********************************
         * get all commodity and assign to variables
         * *******************************/
        for(gii__BarterCatalog__c barterCatalog : [SELECT id,name,giic_External_Id__c,gii__Description__c from gii__BarterCatalog__c where  gii__Type__c='Commodity' limit 50000]){
                (objSalesOrderInfo.commodityName).put(barterCatalog.name,barterCatalog.name);
        }
        
        /*********************************
         * get all brokerprovider  
         * *******************************/
        for(gii__BarterCatalogLine__c barterCataLogLine : [select id,giic_Broker_Provider__c,giic_BrokerProviderSAPId__c,gii__Rate__c from gii__BarterCatalogLine__c where gii__BarterCatalog__r.gii__Type__c='Broker Provider' limit 50000]){
            objSalesOrderInfo.brokerProviderList.add(barterCataLogLine);
            objSalesOrderInfo.mapBrokerRates.put(barterCataLogLine.giic_BrokerProviderSAPId__c, barterCataLogLine);
        }
        
        
        // Getting TaxLocation
        for(gii__TaxLocation__c tLocation : [Select name from gii__TaxLocation__c ORDER BY Name ASC limit 50000]){
                 objSalesOrderInfo.taxLocation.add(tLocation.name);
        }
        System.debug('objSalesOrderInfo******'+objSalesOrderInfo);
        return objSalesOrderInfo;
    }
   /**************************************************
    Method      : getTaxRates
    Purpose     : This Method returns TaxRates on the basis of TaxLocation.
    *****************************************************/
    @AuraEnabled
    public static map<string,object> getTaxRates(map<string,object> inputMap){
        map<string,object> outputMap=new map<string,object>();
        string locationName;
        string commodityName;
        if(inputMap.containsKey('locationName') ){
            locationName=(string)inputMap.get('locationName');
        }
        if(inputMap.containsKey('commodityName')){
            commodityName=(string)inputMap.get('commodityName');
        }
        List<gii__TaxLocation__c> taxLocationList = [Select id,Name,(Select Id,giic_BarterTaxesAvailable__c,giic_BarterTaxesUoM__c,gii__TaxAmount__c,CurrencyISOCode,gii__Jurisdiction__c,gii__Rate__c,giic_Rate_Type__c,giic_Commodity__c from gii__TaxLocationRate__r where giic_Commodity__c=:commodityName) from gii__TaxLocation__c where Name =: locationName limit 50000 ];
        if(taxLocationList != null && !taxLocationList.isEmpty()){
            
          
            outputMap.put('taxLocationMap',new map<id,gii__TaxRate__c>(taxLocationList[0].gii__TaxLocationRate__r));
            outputMap.put('taxLocationList',taxLocationList[0].gii__TaxLocationRate__r);
          
         
        }
        return outputMap; 
    } 
    
    /**************************************************
    Method      : getCommodityInfo
    Purpose     : This Method returns the commodity Information for the commodity selected
    *****************************************************/
    @AuraEnabled
    public static map<string,object> getCommodityInfo(map<string,object> inputMap){
        map<string,object> outputMap=new map<string,object>();
        if(inputMap != null && !inputMap.isEmpty()){
            string offtakerId;
            string serviceProviderId;
            string serviceType;
            string deliveryWarehouseId;
            // Defect Number------6654------------
            string locationName;
            string commodityName;
            // Defect Number------6654------------
            boolean isNutrade;
            if(inputMap.containsKey('offtakerSAPId')){
                offtakerId =(string) inputMap.get('offtakerSAPId');
            }
            if(inputMap.containsKey('serviceProviderId')){
                serviceProviderId =(string) inputMap.get('serviceProviderId');
            }
            if(inputMap.containsKey('serviceType')){
                serviceType =(string) inputMap.get('serviceType');
            }
            if(inputMap.containsKey('isNutrade')){
                isNutrade = (boolean)inputMap.get('isNutrade');
            }
            if(inputMap.containsKey('commodityName')){
                list<string> lstTaxLocation=new list<string>(); 
                set<string> setTaxLocation=new set<string>();
                commodityName = (string)inputMap.get('commodityName');
                System.debug('****commodityName*****'+ commodityName );
                set<string> setCommodityMaterialDivision=new set<string>{'61: Nutrade Specific','60: Barter-OtherOffTaker'};
                map<string,string> mapCommodityType=new map<string,string>();
                map<string,string> mapDeliveryWarehouse = new map<string,string>();
                for(gii__Product2Add__c objProduct : [Select Id,gii__ProductReference__c,gii__ProductReference__r.name,gii__ProductReference__r.Base_UoM__c,gii__ProductReference__r.ProductCode from gii__Product2Add__c where gii__ProductReference__r.Variety_x__c =: commodityName and gii__ProductReference__r.material_division__c in :setCommodityMaterialDivision and gii__ProductReference__r.Global_Life_Cycle_Code__c='LC11: Active' limit 50000]){
                    mapCommodityType.put(objProduct.Id,objProduct.gii__ProductReference__r.Name);
                }
                /*List<gii__TaxRate__c> taxLocationList = [Select Id,gii__TaxLocation__r.name from gii__TaxRate__c where giic_Commodity__c=:commodityName];
                if(taxLocationList != null && !taxLocationList.isEmpty()){
                    for(gii__TaxRate__c objTaxRate : taxLocationList){
                        setTaxLocation.add(objTaxRate.gii__TaxLocation__r.name);
                    }
                    lstTaxLocation.addall(setTaxLocation);
                    outputMap.put('taxLocation',lstTaxLocation);
                }*/
                
                boolean isServiceForSP=false;
                boolean isDeliveryWarehouseForSP=false;
                list<gii__BarterCatalogLine__c> lstCommodityInfo= [select gii__Type__c,giic_OffTaker__c,giic_Is_Surcharge__c,giic_Services__c,giic_DeliveryWarehouse__c,giic_DeliveryWarehouseName__c,giic_LSP__c,gii__UnitofMeasure__c,gii__Rate__c,gii__BarterCatalog__r.giic_External_Id__c from gii__BarterCatalogLine__c where (gii__BarterCatalog__r.giic_External_Id__c=:commodityName and (gii__Type__c = 'Trading Services Fee' or (gii__Type__c='Logistic Service' and giic_LSP__c=:serviceProviderId and giic_Services__c =:serviceType)) )or (gii__BarterCatalog__r.giic_External_Id__c='SERVICEPROVIDER' and giic_LSP__c=:serviceProviderId and gii__Type__c=:'Service Provider' and gii__Rate__c != null) ORDER BY giic_DeliveryWarehouseName__c ASC limit 50000];//added condition "and gii__Type__c = 'Trading Services Fee'" for Incident INC0553908, on 26.04.2016
                for(gii__BarterCatalogLine__c objCommodityInfo : lstCommodityInfo){
                    if(!isNutrade){
                        if(objCommodityInfo.gii__Type__c=='Logistic Service' && objCommodityInfo.giic_LSP__c == serviceProviderId && objCommodityInfo.giic_Services__c == serviceType){
                            isServiceForSP=true;
                            if(objCommodityInfo.giic_DeliveryWarehouse__c != null){
                                isDeliveryWarehouseForSP=true;
                                mapDeliveryWarehouse.put(objCommodityInfo.giic_DeliveryWarehouse__c,objCommodityInfo.giic_DeliveryWarehouseName__c);
                            }
                        }
                        if(objCommodityInfo.gii__BarterCatalog__r.giic_External_Id__c=='SERVICEPROVIDER' && objCommodityInfo.giic_LSP__c==serviceProviderId && objCommodityInfo.gii__Type__c=='Service Provider' ){
                             outputMap.put('LogisticsServiceFee',objCommodityInfo.gii__Rate__c);
                        }
                    }
                    if(objCommodityInfo.giic_OffTaker__c==offtakerId && objCommodityInfo.gii__Type__c == 'Trading Services Fee' && objCommodityInfo.giic_Is_Surcharge__c && objCommodityInfo.gii__Rate__c != null)
                        outputMap.put('TradingFeeSurchargeRate',objCommodityInfo.gii__Rate__c);
                    if(objCommodityInfo.giic_OffTaker__c==offtakerId && objCommodityInfo.gii__Type__c == 'Trading Services Fee' && !objCommodityInfo.giic_Is_Surcharge__c && objCommodityInfo.gii__Rate__c != null)
                        outputMap.put('AccrualTradingFeeSurchargeRate',objCommodityInfo.gii__Rate__c);
                }    
                outputMap.put('DeliveryWarehouse',mapDeliveryWarehouse);
                if(!isServiceForSP && !isNutrade && !string.isBlank(serviceProviderId)){
                    outputMap.put('ErrorMessage','No Service found for the selected commodity. Please select another service.');
                }
                else if(!isDeliveryWarehouseForSP && !isNutrade && !string.isBlank(serviceProviderId)){
                    outputMap.put('ErrorMessage','No Delivery Warehouse found for the service with this commodity. Please select amother service.');
                }
                outputMap.put('CommodityType',mapCommodityType);
                 List<string> lstAllUOM = new List<string>();
                Set<string> setAlternateUOM = new Set<string>();
                string baseUoM='';
                list<gii__ProductUnitofMeasureConversion__c> lstProductUOM = [select id,gii__ConversionFactor__c,CurrencyIsoCode,gii__UnitofMeasureIn__r.Name,gii__UnitofMeasureOut__r.Name,gii__Product__c from gii__ProductUnitofMeasureConversion__c where gii__Product__r.gii__ProductReference__r.Variety_x__c =: commodityName limit 50000];
                for(gii__ProductUnitofMeasureConversion__c objProductUOM : lstProductUOM){
                    setAlternateUOM.add(objProductUOM.gii__UnitofMeasureOut__r.Name);
                    baseUoM=objProductUOM.gii__UnitofMeasureOut__r.Name;
                    string sKey=commodityName+''+objProductUOM.gii__UnitofMeasureIn__r.Name + '' + objProductUOM.gii__UnitofMeasureOut__r.Name;
                    outputMap.put('CommodityUOMConversion',new map<string,decimal>{sKey => objProductUOM.gii__ConversionFactor__c});
                    
                }
                setAlternateUOM.add(baseUoM);   
                lstAllUOM.addAll(setAlternateUOM);
                system.debug('***lstAllUOM****'+lstAllUOM);
                outputMap.put('AllUoM',lstAllUOM);
                outputMap.put('BaseUoM',baseUoM);
            }
            
            // Location -------------6654-------
            if(inputMap.containsKey('locationName')){
                locationName = (string)inputMap.get('locationName');
                
                if(!String.isBlank(locationName) && !String.isBlank(commodityName)){
                    map<string,object> returnTaxes = getTaxRates(new map<string,object>{'locationName'=>locationName,'commodityName'=>commodityName});
                    outputMap.putAll(returnTaxes);
                }
            }
            // Location -------------6654-------
        }    
        System.debug('****outputMap*****'+ outputMap );
        return outputMap;
    }
    /**************************************************
    Method      : getCommodityTypeInfo
    Purpose     : This Method returns commodity information based on the commodity and commodity type
    *****************************************************/
    @AuraEnabled
    public static map<string,object> getCommodityTypeInfo(map<string,object> inputMap){
        System.debug('****inputMap*****'+ inputMap );
        string commodityTypeId;
        string delWarehouseId;
        string certificationNumber;
        string harvestRegion;
        string priceSeason;
        string offTakerId;
        string commodityName;
        string serviceProviderId;
        string serviceType;
        boolean isNutrade=false;
        string soCurrency;
        string uom;
        if(inputMap.containsKey('commodityTypeId'))
            commodityTypeId=(string)inputMap.get('commodityTypeId');
        if(inputMap.containsKey('delWarehouseId'))
            delWarehouseId=(string)inputMap.get('delWarehouseId');
        if(inputMap.containsKey('certificationNumber'))
            certificationNumber=(string)inputMap.get('certificationNumber');
        if(inputMap.containsKey('harvestRegion'))
            harvestRegion=(string)inputMap.get('harvestRegion');
        if(inputMap.containsKey('priceSeason'))
            priceSeason=(string)inputMap.get('priceSeason');
        if(inputMap.containsKey('offTakerId'))
            offTakerId=(string)inputMap.get('offTakerId');
        if(inputMap.containsKey('commodityName')){
            commodityName = (string)inputMap.get('commodityName');
        }  
        if(inputMap.containsKey('serviceProviderId'))
            serviceProviderId=(string)inputMap.get('serviceProviderId');
        if(inputMap.containsKey('serviceType'))
            serviceType=(string)inputMap.get('serviceType');
        if(inputMap.containsKey('isNutrade')){
            isNutrade = (boolean)inputMap.get('isNutrade');
        } 
       
        if(inputMap.containsKey('soCurrency')){
            soCurrency = (string)inputMap.get('soCurrency');
        } 
        if(inputMap.containsKey('uom')){
            uom = (string)inputMap.get('uom');
            
        }
        system.debug(uom+'***' + soCurrency);
        system.debug(certificationNumber+'****'+offtakerId + '***' + commodityTypeId + '**' + priceSeason + '***'+commodityName+'***'+serviceProviderId+'****'+serviceType+'****'+delWarehouseId+'***'+harvestRegion);
        map<string,object> mapCommodityInfo=new map<string,object>(); 
        mapCommodityInfo.put('PriceSeason',new set<string>());
        mapCommodityInfo.put('HarvestReason',new set<string>());
        mapCommodityInfo.put('CertificationNumber',new set<string>());
        mapCommodityInfo.put('DeliveryWarehouse',new map<string,string>());
        mapCommodityInfo.put('CommodityUOMConversion',new map<string,decimal>());
        mapCommodityInfo.put('CurrencyConversion',new map<string,decimal>());
        
        if(!isNutrade && serviceProviderId != null && serviceType != null){
            list<gii__BarterCatalogLine__c> lstLogisticsService= [select gii__Type__c,gii__UnitPrice__c,giic_OffTaker__c,CurrencyIsoCode,giic_Is_Surcharge__c,giic_Services__c,giic_DeliveryWarehouse__c,giic_DeliveryWarehouseName__c,giic_LSP__c, gii__UnitofMeasure__c,gii__UnitofMeasure__r.Name,gii__Rate__c,gii__BarterCatalog__r.giic_External_Id__c from gii__BarterCatalogLine__c where gii__BarterCatalog__r.giic_External_Id__c=:commodityName and giic_LSP__c =:serviceProviderId and giic_Services__c=:serviceType and giic_DeliveryWarehouse__c=:delWarehouseId and gii__Type__c = 'Logistic Service' limit 50000];
            if(lstLogisticsService != null && lstLogisticsService.size() > 0){
                mapCommodityInfo.put('LogisticsService',lstLogisticsService[0].gii__UnitPrice__c);
                mapCommodityInfo.put('LogisticsServiceCurrency',lstLogisticsService[0].CurrencyIsoCode);
                mapCommodityInfo.put('LogisticsServiceUOM',lstLogisticsService[0].gii__UnitofMeasure__r.Name);
            }
        }
        
        
        set<Id> setWarehouseId=new set<Id>();
        list<gii__Product2Add__c> lstProductReference = [select id,gii__ProductReference__c,gii__ProductReference__r.Base_UoM__c from gii__Product2Add__c where Id=:commodityTypeId limit 50000];
        System.debug('****lstProductReference*****'+ lstProductReference );
        if(lstProductReference != null && lstProductReference.size() > 0){
            date dtToday=system.today();
            /*if(!(uom != null && uom != ''))
                uom=lstProductReference[0].gii__ProductReference__r.Base_UoM__c;*/
            system.debug('***********UOM***'+lstProductReference[0].gii__ProductReference__r.Base_UoM__c);
            
            string sQuery='select id,giic_Price_Season__c,gii__UnitPrice__c,    gii__UnitofMeasure__c,CurrencyIsoCode,giic_DeliveryWarehouse__c,giic_DeliveryWarehouseName__c,giic_Certification_Number__c,giic_Harvest_Region__c from gii__BarterCatalogLine__c where gii__Type__c=\'Commodity Reference Price\' and gii__Product__c=:commodityTypeId ' + (priceSeason != '' ? ' and giic_Price_Season__c=:priceSeason ' :'') +
                                                       +  (delWarehouseId != '' ? ' and giic_DeliveryWarehouse__c=:delWarehouseId ' : '') + (harvestRegion != '' ? ' and giic_Harvest_Region__c=:harvestRegion ' :'') + (certificationNumber != '' ? ' and giic_Certification_Number__c=:certificationNumber' : '') + (offTakerId != '' ? ' and giic_OffTaker__c=:offTakerId ' :'' ) + (commodityName != '' ? ' and gii__BarterCatalog__r.Name=:commodityName ' :'' ) + (soCurrency != null ? ' and currencyisocode =:soCurrency ' : '' )  + ' and giic_PriceDate__c =:dtToday' ;  // ' and giic_PriceDate__c =:dtToday' 
            System.debug('****sQuery*****'+ sQuery );
            //commodityTypeId=lstProductReference[0].id;
            map<string,string> mapDeliveryWarehouse = new map<string,string>();
           /* list<string> lstAlternateUOM=new list<string>();
            list<gii__ProductUnitofMeasureConversion__c> lstProductUOM = [select id,gii__ConversionFactor__c,gii__UnitofMeasureOut__c from gii__ProductUnitofMeasureConversion__c where gii__UnitofMeasureIn__c=:lstProductReference[0].gii__ProductReference__r.Base_UoM__c];
            for(gii__ProductUnitofMeasureConversion__c objProductUOM : lstProductUOM){
                lstAlternateUOM.add(objProductUOM.gii__UnitofMeasureOut__c);
            }   */      
            
            
            
            // Currency Conversion
            map<string,object> mapConversionRatesOutput=giic_PricingHelper.getConversionRates();
            
            map<string,double> mapConversionRates=(map<string,double>)mapConversionRatesOutput.get('CONVERSIONRATES'); 
            string sCorporateCurrency=(string) mapConversionRatesOutput.get('CORPORATECURRENCY');
            if(sCorporateCurrency != null){
                mapCommodityInfo.put('CorporateCurrency',sCorporateCurrency);
            }
           
            if(mapConversionRates != null){
                if(soCurrency != sCorporateCurrency){
                    mapCommodityInfo.put('CurrencyConversion',new map<string,decimal>{soCurrency => mapConversionRates.get(soCurrency)});
                }else{
                     mapCommodityInfo.put('CurrencyConversion',new map<string,decimal>{soCurrency => mapConversionRates.get(sCorporateCurrency)});
                }
            }
            /*List<string> lstAllUOM = new List<string>();
            Set<string> setAlternateUOM = new Set<string>();
            string baseUoM=lstProductReference[0].gii__ProductReference__r.Base_UoM__c;
            list<gii__ProductUnitofMeasureConversion__c> lstProductUOM = [select id,gii__ConversionFactor__c,CurrencyIsoCode,gii__UnitofMeasureIn__r.Name,gii__UnitofMeasureOut__r.Name,gii__Product__c from gii__ProductUnitofMeasureConversion__c where gii__Product__r.gii__ProductReference__r.Variety_x__c =: commodityName and gii__Product__c=:commodityTypeId];
            for(gii__ProductUnitofMeasureConversion__c objProductUOM : lstProductUOM){
                setAlternateUOM.add(objProductUOM.gii__UnitofMeasureOut__r.Name);
                baseUoM = objProductUOM.gii__UnitofMeasureOut__r.Name;
                //setAlternateUOM.add(objProductUOM.gii__UnitofMeasureIn__r.Name);
                string sKey=commodityName+''+objProductUOM.gii__UnitofMeasureIn__r.Name + '' + objProductUOM.gii__UnitofMeasureOut__r.Name;
                mapCommodityInfo.put('CommodityUOMConversion',new map<string,decimal>{sKey => objProductUOM.gii__ConversionFactor__c});
                
            }
            setAlternateUOM.add(baseUoM);   
            lstAllUOM.addAll(setAlternateUOM);
            system.debug('***lstAllUOM****'+lstAllUOM);
            mapCommodityInfo.put('AllUoM',lstAllUOM);
            mapCommodityInfo.put('BaseUoM',baseUoM);*/
            boolean isUnitPrice=false;
            boolean isPriceMaintain=false;
            /*if(baseUoM == 'SAC'){
               baseUoM='BAG';
            }*/
            sQuery += ( uom != null ? ' and gii__UnitofMeasure__r.name =: uom ' :'' + ' ORDER BY giic_DeliveryWarehouseName__c ASC ' );
            system.debug('--------final Query----'+sQuery);
            for(gii__BarterCatalogLine__c objPriceEntry : database.query(sQuery)){
                isPriceMaintain=true;
                System.debug('***objPriceEntry ****'+ objPriceEntry);
                ((set<string>)mapCommodityInfo.get('PriceSeason')).add(objPriceEntry.giic_Price_Season__c);
                ((set<string>)mapCommodityInfo.get('CertificationNumber')).add(objPriceEntry.giic_Certification_Number__c);
                ((set<string>)mapCommodityInfo.get('HarvestReason')).add(objPriceEntry.giic_Harvest_Region__c);
                if(!isUnitPrice){
                    System.debug('***certificationNumber ****'+ certificationNumber);
                    System.debug('***objPriceEntry.gii__UnitPrice__c ****'+ objPriceEntry.gii__UnitPrice__c);
                    if(harvestRegion != '' && priceSeason != '' && delWarehouseId != '' && certificationNumber != '' && certificationNumber == objPriceEntry.giic_Certification_Number__c){
                        mapCommodityInfo.put('UnitPrice',objPriceEntry.gii__UnitPrice__c);
                        isUnitPrice=true;
                    }
                    else if(harvestRegion != '' && priceSeason != '' && delWarehouseId != '' && certificationNumber == '' && string.isBlank(certificationNumber) == string.isBlank(objPriceEntry.giic_Certification_Number__c)){
                        mapCommodityInfo.put('UnitPrice',objPriceEntry.gii__UnitPrice__c);
                        isUnitPrice=true;
                    }
                    else{
                        mapCommodityInfo.put('UnitPrice', 0.00); 
                    }
                }
                mapDeliveryWarehouse.put(objPriceEntry.giic_DeliveryWarehouse__c,objPriceEntry.giic_DeliveryWarehouseName__c);
            }
            if(!isPriceMaintain && isNutrade)
                mapCommodityInfo.put('Message',system.label.giic_NutradePriceNotMaintain);
            mapCommodityInfo.put('DeliveryWarehouse',mapDeliveryWarehouse);
        }
     
        system.debug('****mapCommodityInfo****'+mapCommodityInfo);
        return mapCommodityInfo;
    }
    
   /**************************************************
    Method      : getBrokerProvider
    Purpose     : This Method is used to get all the Broker Provider
    *****************************************************/
    @AuraEnabled
    public static list<gii__BarterCatalogLine__c> getBrokerProvider(){
        list<gii__BarterCatalogLine__c > lstBarterCatalogLines= [select id,giic_Broker_Provider__c,giic_BrokerProviderSAPId__c,gii__Rate__c from gii__BarterCatalogLine__c where gii__BarterCatalog__r.gii__Type__c='Broker Provider' limit 50000];
        
        return lstBarterCatalogLines;
    }
    
    /**************************************************
    Method      : getBrokerProviderPercentRate
    Purpose     : This Method is used to get Broker Provider Percentage rate
    *****************************************************/
    @AuraEnabled
    public static String getBrokerProviderPercentageRate(String brokerPvdId){
        Id borkerProviderId = Id.valueOf(brokerPvdId);
        System.debug('***borkerProviderId***'+ borkerProviderId);
        gii__BarterCatalogLine__c barterCatalogLine = [Select id,giic_BrokerProviderSAPId__c,gii__Rate__c from gii__BarterCatalogLine__c where id =: borkerProviderId limit 50000];
        if(barterCatalogLine!= null ){
            System.debug('***borkerPriceRate***'+ String.valueOf(barterCatalogLine.gii__Rate__c));
            return String.valueOf(barterCatalogLine.gii__Rate__c);
        }
        return null;
    } 
    
    /**************************************************
    Method      : createSalesOrder
    Purpose     : This Method is used to create the sales order and sales order payment record
    *****************************************************/
    @AuraEnabled
    public static giic_SalesOrderMasterWrapper createSalesOrder(String SOWrapper,String SOPayment){
        giic_SalesOrderMasterWrapper objSalesOrderMasterWrapper = giic_CreateSalesOrderHeaderCtrl.processSO_V1(SOWrapper);
        system.debug('***SOPayment**'+SOPayment);
        gii__SalesOrderPayment__c objSOPayment = (gii__SalesOrderPayment__c)JSON.deserialize(SOPayment, gii__SalesOrderPayment__c.class);
        if(objSalesOrderMasterWrapper.sowithfamiltlist != null && objSalesOrderMasterWrapper.sowithfamiltlist.size() > 0){
            // Code Added for CR-532 SO Review Convert SQ to Sales order 
            if(objSalesOrderMasterWrapper.SQIsClone == FALSE 
               && (objSalesOrderMasterWrapper.soPaymentSize > 0 || objSOPayment.gii__SalesOrder__c != null)){
                System.debug('***********Sales Order payment Update*****'+ objSOPayment);
                update objSOPayment;
            }else{
                System.debug('***********Sales Order payment Insert*****'+ objSOPayment + 'soValue***' + objSOPayment.gii__SalesOrder__c);
                gii__SalesOrderPayment__c objSOPToInsert=new gii__SalesOrderPayment__c();
                objSOPToInsert=objSOPayment.clone(false,false);
                objSOPToInsert.CurrencyIsoCode=[select currencyisocode from gii__SalesOrder__c where id=:objSalesOrderMasterWrapper.sowithfamiltlist[0].oSalesOrder.id][0].currencyisocode;
                objSOPToInsert.gii__SalesOrder__c=objSalesOrderMasterWrapper.sowithfamiltlist[0].oSalesOrder.id;
                insert objSOPToInsert;   
            }
        }
        return objSalesOrderMasterWrapper;
    }
    /**************************************************
    Method      : getBarterTemplate
    Purpose     : This Method is used to get Sales Order Payment Description
    *****************************************************/
    @AuraEnabled
    public static list<gii__SalesOrderPayment__c> getBarterTemplateInfo(){
        return getSalesOrderPaymentInfo(new map<string,object>());
    }
    /**************************************************
    Method      : getSalesOrderPaymentInfo
    Purpose     : This Method is used to get Sales Order Payment Information
    *****************************************************/
    @AuraEnabled
    public static list<gii__SalesOrderPayment__c> getSalesOrderPaymentInfo(map<string,object> mapInput){
        string salesOrderId='';
        string salesOrderPaymentId='';
        
        string sQuery='Select Id,giic_TotalSalesTaxesRate__c,giic_CommodityType__c,giic_ServiceProviderName__c,giic_DeliveryWarehouseName__c,giic_Offtakername__c,giic_Commodity_Actual_Gross_Price__c,giic_ApprovedCurrCommGP__c,giic_New_SO_With_Taxes_and_Interest__c,giic_Commodity_UoM__c,giic_BRComments__c,gii__Comments__c,giic_BrokerServiceFeeSurcharge__c,giic_Barter_Template_Id__c,giic_SurchargeTaxesCmUOM__c,giic_TradingFeeSurcharge__c,giic_OrderValueAfterOpenPackageReductor__c,giic_Harvest_Region_State__c,giic_Barter_Taxes_Value_UoM_Applied__c,'+
                +'giic_Barter_Taxes_Value_UoM__c,giic_Barter_Reference_Value__c,giic_SO_Without_Tax_With_Interest__c,giic_Barter_Surcharges_Subtotal__c,giic_Logistics_Service_Surcharge__c,giic_Logistics_Service_Fee_Surcharge__c,giic_Delivery_Warehouse__c,giic_Is_Broker_Service__c,'+
                +'giic_Distributor_Margin_Difference__c,giic_deliverywarehouse__c,giic_Price_Season__c,giic_ServiceType__c,giic_ServiceProvider__c,giic_BarterTax__c,giic_CommodityTypeCode__c,giic_Barter_Taxes_Rate__c,giic_Barter_Taxes_Applied__c,giic_ContractPriceAgreement__c,giic_HarvestRegion__c, giic_Certification__c, '+
                +'giic_Grower_Doc_Number__c, giic_Grower_Name__c,giic_OrderValueAftPackRedwoInterest__c,giic_TotalOff_taker_Purchase_Gross_Value__c,giic_Total_Off_taker_Purchase_Net_Value__c,giic_Broker_Provider__c,giic_BrokerProviderName__c, giic_Commodity_Reference_Gross_Price__c,giic_Unit_Reference_gross_Price__c,giic_Logistic_Services_Cm_UoM__c,giic_Logistic_Services_Fee_Cm_UoM__c,giic_BrokerServicesFeeCmUOM__c,giic_Trading_Fee_as_Accrual__c,'+
                +'giic_Trading_Fee_Surcharge_Cm_UoM__c,giic_AvgCustSegDisc__c,giic_Unit_of_Measure_Round_Up__c,giic_Barter_Financ_Rebate__c,giic_Barter_Letter_of_Credit_Value__c,giic_Commodity_Price_Variance__c, giic_Commodity_Price_Variance_Net_Value__c,giic_ActualgrossPrice__c,giic_Commodity_Reference_Net_Price__c,giic_Commodity_Volumes__c,giic_Commodity_Volumes_Rounded__c,giic_OffTaker__c,giic_BarterModel__c, giic_Commodity_Name__c,giic_SO_With_Tax_And_Interest__c ';
        string sWhereCondition='';
        if(mapInput.containsKey('salesOrderId') && mapInput.get('salesOrderId') != null && mapInput.get('salesOrderId') != ''){
            salesOrderId = (string)mapInput.get('salesOrderId') ;
            sWhereCondition =' where gii__SalesOrder__c =:salesOrderId ' ;
        }
        else if(mapInput.containsKey('salesOrderPaymentId') && mapInput.get('salesOrderPaymentId') != null && mapInput.get('salesOrderPaymentId') != ''){
            salesOrderPaymentId = (string)mapInput.get('salesOrderPaymentId') ;
            sWhereCondition =' where Id =:salesOrderPaymentId order by giic_TemplateSequence__c asc' ;
        } 
        else{
            sWhereCondition =' where gii__SalesOrder__r.gii__Account__r.Name =\'' +  System.Label.giic_BarterDummyAccount + '\' order by giic_TemplateSequence__c asc';
        }
        string sFinalQuery=sQuery + ' From gii__SalesOrderPayment__c ' + sWhereCondition;
        return (list<gii__SalesOrderPayment__c>) database.query(sFinalQuery);
    }
    /**************************************************
    Method      : getIntializeSOP
    Purpose     : This Method is used to intialize fields for Sales Order Payment
    *****************************************************/
    public static gii__SalesOrderPayment__c getIntializeSOP(){
        gii__SalesOrderPayment__c objSalesOrderPayment = new gii__SalesOrderPayment__c(giic_TotalSalesTaxesRate__c=0,giic_CommodityType__c=null,giic_Offtakername__c='',giic_Commodity_Actual_Gross_Price__c=0.00,giic_OrderValueAftPackRedwoInterest__c=0.00,giic_ApprovedCurrCommGP__c=0.00,giic_New_SO_With_Taxes_and_Interest__c=0.00,giic_Commodity_UoM__c='',giic_CommodityTypeCode__c='',giic_Commodity_Reference_Gross_Price__c=0.00,giic_BrokerServiceFeeSurcharge__c=0.00,giic_Barter_Template_Id__c='',giic_SurchargeTaxesCmUOM__c=0.00,giic_TradingFeeSurcharge__c=0.00,giic_OrderValueAfterOpenPackageReductor__c=0.00,giic_Harvest_Region_State__c='',giic_Barter_Taxes_Value_UoM_Applied__c='',giic_Barter_Taxes_Value_UoM__c=0.00,giic_Barter_Reference_Value__c=0,giic_SO_Without_Tax_With_Interest__c=0.00,giic_Barter_Surcharges_Subtotal__c=0.00,giic_Logistics_Service_Surcharge__c=0.00,giic_Logistics_Service_Fee_Surcharge__c=0.00,giic_Delivery_Warehouse__c='',giic_Is_Broker_Service__c=false,giic_Distributor_Margin_Difference__c=0.00,giic_deliverywarehouse__c=null,giic_Price_Season__c='',giic_ServiceType__c='', giic_ServiceProvider__c='',giic_BarterTax__c='',giic_Barter_Taxes_Rate__c =0.0,giic_Barter_Taxes_Applied__c='',giic_ContractPriceAgreement__c='',giic_DeliveryWarehouseName__c='',giic_ServiceProviderName__c='',giic_HarvestRegion__c= '', giic_Certification__c='', giic_Grower_Doc_Number__c='', giic_Grower_Name__c='',giic_TotalOff_taker_Purchase_Gross_Value__c= 0.00,giic_Total_Off_taker_Purchase_Net_Value__c=0.00,giic_Broker_Provider__c='', giic_BrokerProviderName__c='',giic_Unit_Reference_gross_Price__c= 0.0,giic_Logistic_Services_Cm_UoM__c=0.00,giic_Logistic_Services_Fee_Cm_UoM__c=0.00,giic_BrokerServicesFeeCmUOM__c=0.00,giic_Trading_Fee_as_Accrual__c=0.00,giic_Trading_Fee_Surcharge_Cm_UoM__c=0.00,giic_Unit_of_Measure_Round_Up__c=0.00,giic_Barter_Financ_Rebate__c=0.00,giic_Barter_Letter_of_Credit_Value__c=0.00,giic_Commodity_Price_Variance__c=0.00, giic_Commodity_Price_Variance_Net_Value__c=0.00,giic_ActualgrossPrice__c=0.0,giic_Commodity_Reference_Net_Price__c=0.0,giic_Commodity_Volumes__c =0.0,giic_Commodity_Volumes_Rounded__c=0 ,giic_OffTaker__c='',giic_BarterModel__c='', giic_Commodity_Name__c='');
        return objSalesOrderPayment;
    }
    public static set<string> getStateByCountry(){
        List<Schema.PicklistEntry> ple = User.statecode.getDescribe().getPicklistValues();
        System.debug('Picklist::'+ple);
        set<string> setStateCodes=new set<string>();
        for( Schema.PicklistEntry f : ple){
            System.debug(f.getLabel() +'::'+ f.getValue());
            setStateCodes.add(f.getValue());
        }
        system.debug('***setStateCodes**'+setStateCodes);
        return setStateCodes;
    }
}