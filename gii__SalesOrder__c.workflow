<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
	<outboundMessages>
        <fullName>OutboundSOMsgToSAP</fullName>
        <apiVersion>38.0</apiVersion>
        <description>Send an Outbound Message to SAP pertaining to Sales Order</description>
        <endpointUrl>${label_SALESORDER_SAP_URL}</endpointUrl>
        <fields>Id</fields>
        <fields>giic_IntegrationOperation__c</fields>
        <fields>giic_OrderTypeCode__c</fields>
        <fields>giic_OrderTypeValue__c</fields>
        <fields>giic_SalesOrderSource__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>prabakaran.pakkam@syngenta.aol</integrationUser>
        <name>OutboundSOMsgToSAP</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>	
    <rules>
        <fullName>SAP Order Submit Action</fullName>
        <actions>
            <name>OutboundSOMsgToSAP</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>gii__SalesOrder__c.giic_ProcessingStatus__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>gii__SalesOrder__c.giic_SalesOrderSource__c</field>
            <operation>contains</operation>
            <value>SAP FEP BR,GLOVIA BR,MobileApp BR,SAP FFU BR</value>
        </criteriaItems>
        <description>This Workflow rule is fired when Sales Order Processing status is set to  &apos;In Progress&apos;. It will triger outbound message which will send SFDC id and other fields to SAP Middleware.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>OET BR Sales Order SAP Update</fullName>
        <actions>
            <name>giic_OET_BR_Sales_Order_Amount_Difference_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This Workflow rule will be fired when a sales quote converted into sales order and sales order is updated from SAP with different Gross Amount than Sales Quote for BR92 Sales Org</description>
        <formula>AND(giic_SalesOrg__c  = $Label.giic_sorg_name,  ISCHANGED(giic_SAPOrderNum__c), giic_BRSalesQuoteGrossAmount__c  &lt;&gt; gii__NetAmount__c,  NOT(ISBLANK( gii__SalesQuote__c ) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <alerts>
        <fullName>giic_OET_BR_Sales_Order_Amount_Difference_Email_Alert</fullName>
        <description>OET BR Sales Order Amount Difference Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>giic_OET_BR_Sales_Order_Operations_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/giic_OET_BR_Sales_Order_Amount_Diff_Email_Notification_Email_Template</template>
    </alerts>
	<fieldUpdates>
        <fullName>Update_CA_Approved_Checkbox</fullName>
        <field>giic_CA_Discount_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Update CA Approved Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>giic_CA_Discount_Approved_Flag_Reset</fullName>
        <field>giic_CA_Discount_Approved__c</field>
        <literalValue>0</literalValue>
        <name>CA Discount Approved Flag Reset</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>giic_CA_Discount_Approved_Flag_Set</fullName>
        <field>giic_CA_Discount_Approved__c</field>
        <literalValue>1</literalValue>
        <name>CA Discount Approved Flag Set</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>giic_PopulateDocumentDateForNull</fullName>
        <description>Populate Document Date for Sales Order with Created Date when it is null</description>
        <field>giic_Document_Date__c</field>
        <formula>DATEVALUE(CreatedDate )</formula>
        <name>giic_PopulateDocumentDateForNull</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
	 <fieldUpdates>
        <fullName>Uncheck_CA_Approved</fullName>
        <field>giic_CA_Discount_Approved__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck CA Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_CA_Rejected</fullName>
        <field>giic_CA_Discount_Approved__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck CA Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>giic_Clear_SO_CA_Approved</fullName>
        <field>giic_CA_Discount_Approval_Status__c</field>
        <name>Clear SO CA Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>giic_Mark_SO_CA_Appproved</fullName>
        <field>giic_CA_Discount_Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Mark SO CA Appproved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>giic_Update_Approval_Status_Approved</fullName>
        <description>Update Approval Status to Approved</description>
        <field>giic_ApprovalStatus__c</field>
        <literalValue>Approved</literalValue>
        <name>Update Approval Status Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>giic_Update_Approval_Status_Rejected</fullName>
        <description>Update Approval Status to Rejected</description>
        <field>giic_ApprovalStatus__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update Approval Status Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>giic_Update_Approval_Status_To_Pending</fullName>
        <description>Update Approval Status To Pending</description>
        <field>giic_ApprovalStatus__c</field>
        <literalValue>Pending</literalValue>
        <name>Update Approval Status To Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>giic_Update_Approval_Status_To_Saved</fullName>
        <description>Update Approval Status To Saved</description>
        <field>giic_ApprovalStatus__c</field>
        <literalValue>Saved</literalValue>
        <name>Update Approval Status To Saved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>giic_Update_Processing_Status</fullName>
        <description>Update processing status to in progress when order is approved/rejected</description>
        <field>giic_ProcessingStatus__c</field>
        <literalValue>In Progress</literalValue>
        <name>Update Processing Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>